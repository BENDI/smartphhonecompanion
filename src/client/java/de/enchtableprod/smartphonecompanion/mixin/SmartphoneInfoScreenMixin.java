package de.enchtableprod.smartphonecompanion.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import de.enchtableprod.smartphonecompanion.SmartphoneInfoScreen;
import net.minecraft.client.gui.screen.ChatScreen;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextIconButtonWidget;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

@Mixin(ChatScreen.class)
public abstract class SmartphoneInfoScreenMixin extends Screen {

    protected SmartphoneInfoScreenMixin() {
        super(Text.literal("shit"));
    }

    public TextIconButtonWidget createSmartphoneButton() {
        TextIconButtonWidget sb = TextIconButtonWidget.builder(Text.literal("Smartphone"), button -> {
            this.client.setScreen(new SmartphoneInfoScreen(this));
        }, true)
                .dimension(20, 20)
                .texture(new Identifier("smartphhonecompanion:smartphonebutton"), 16, 16)
                .build();
        sb.setPosition(this.width - 23, this.height - 35);
        return sb;
    }

    @Inject(at = @At("HEAD"), method = "init")
    protected void init(CallbackInfo info) {
        // this.addDrawableChild(ButtonWidget.builder(Text.literal("penis"), button -> {
        // this.client.setScreen(new SmartphoneInfoScreen(this));
        // }).dimensions(this.width - 25, this.height - 35, 20, 20).build());s
        this.addDrawableChild(createSmartphoneButton());
    }

}
