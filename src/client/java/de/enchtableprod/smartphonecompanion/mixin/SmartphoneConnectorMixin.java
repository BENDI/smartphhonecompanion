package de.enchtableprod.smartphonecompanion.mixin;

import org.spongepowered.asm.mixin.Mixin;

import de.enchtableprod.smartphonecompanion.ISmartphoneConnector;
import de.enchtableprod.smartphonecompanion.SmartphoneConnector;
import net.minecraft.client.MinecraftClient;

@Mixin(MinecraftClient.class)
public abstract class SmartphoneConnectorMixin implements ISmartphoneConnector {

    private SmartphoneConnector sc;

    public SmartphoneConnector connectSmartphone() {
        this.sc = new SmartphoneConnector();
        return this.sc;
    }

}
