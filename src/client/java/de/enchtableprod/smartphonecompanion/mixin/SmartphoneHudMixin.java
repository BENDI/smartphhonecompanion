package de.enchtableprod.smartphonecompanion.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.hud.InGameHud;
import net.minecraft.util.Identifier;

@Mixin(InGameHud.class)
public abstract class SmartphoneHudMixin {

    private final Identifier SMARTPHONE_HUD_TEXTURE = new Identifier("smartphhonecompanion:smartphonebutton");

    @Inject(at = @At("TAIL"), method = "renderHotbar", locals = LocalCapture.CAPTURE_FAILHARD)
    private void renderHotbar(float tickDelta, DrawContext context, CallbackInfo info) {
        context.drawGuiTexture(SMARTPHONE_HUD_TEXTURE,
                context.getScaledWindowWidth() - 23, context.getScaledWindowHeight() - 35, 16, 16);
    }
}
