package de.enchtableprod.smartphonecompanion;

public class SmartphoneConnector {
    private String uuid = "0x001";

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

}
