package de.enchtableprod.smartphonecompanion;

public interface ISmartphoneConnector {

    public SmartphoneConnector connectSmartphone();

}
