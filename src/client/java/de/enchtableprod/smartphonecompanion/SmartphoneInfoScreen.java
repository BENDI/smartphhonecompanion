package de.enchtableprod.smartphonecompanion;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.tooltip.Tooltip;
import net.minecraft.client.gui.widget.ButtonWidget;
import net.minecraft.text.Text;

@Environment(EnvType.CLIENT)
public class SmartphoneInfoScreen extends Screen {

    private Screen parent;

    public SmartphoneInfoScreen(Screen parent) {
        super(Text.translatable("screen.smartphoneinfoscreen.title"));
        this.parent = parent;
    }

    public ButtonWidget button1;
    public ButtonWidget button2;

    @Override
    protected void init() {
        button1 = ButtonWidget.builder(Text.literal("button1"), button -> {
            System.out.println(((ISmartphoneConnector) this.client).connectSmartphone().getUuid());
        }).dimensions(width / 2, 20, 30, 20)
                .tooltip(Tooltip.of(Text.literal("tooltip"))).build();
        addDrawableChild(button1);
    }

    @Override
    public void close() {
        client.setScreen(parent);
    }
}
